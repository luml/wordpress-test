<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'test-wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'yJu)]VWTMR/a+vODeU=1;}k5mbuYOJBB/G?>iH(4wm5xu*- Vh/a74Hf(8PFBp7s' );
define( 'SECURE_AUTH_KEY',  'q+C7TLw}Mi<(}a RM|)>y()KuWzoLz(N0<&`0oH3G]15zXPjU%KEByWD^{43:}+%' );
define( 'LOGGED_IN_KEY',    'u,U0mn*k]`,~U^T:1xFGzn`&o%=>BA5O9;Zuopx[#3yBFJZ}5T;j}ta]Cqpu2G7)' );
define( 'NONCE_KEY',        '>t)r+!ADGBa(1qWu8>=Vt^EM{Z[c9/(al8`u2PhL7!`S5-W.FM{<0B2f!=9Lyy.D' );
define( 'AUTH_SALT',        ' +6](gOpPTw-hsHIL[&2<UjCeOI<hQD[x?KSp1(S&o1v d*~xp!U_9k=4w-%pb$+' );
define( 'SECURE_AUTH_SALT', 'B&SVA$&WHYJH]T]z:681bmNz#P!qj0QHidN-PEdlYZQSPDW:%P9wnGU)JIYwT^`:' );
define( 'LOGGED_IN_SALT',   'N8y61HTQ{NIzKIJt|1bU^1kb*_sYP-@if3$r`IDW@x7:PVM]AqQ(PD~yIKTVN}dI' );
define( 'NONCE_SALT',       ']Uv&Bh/T9aaNx1Yd*?5oDaFRGanf(TXVm S5.~/2{]#DIRZRMVkXG-|aeo5TlXn;' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
